# This script generates a single csv for either a host or a component
source properties.sh
    
if [ -d csvFiles ]; then
  rm -rf csvFiles
fi
if [ -d queryResults ]; then
  rm -rf queryResults
fi

mkdir -p csvFiles/hostFiles
mkdir -p csvFiles/componentFiles
mkdir queryResults

#<------------------------------------------------------------------>
#                      Fetch distinct host names
#<------------------------------------------------------------------>

echo "Quering the elastic index heal-hnf-txn-* and fetching the list of distinct services"
curl -XPOST -H 'Content-Type: application/json' $elasticUrl$indexPattern"/_search?pretty" -d'
{
   "size":0,
   "query":{
      "bool":{
         "must":[
            {
               "range":{
                  "HNFKPI.TimestampFrom":{
                     "gte":"'${startDate}'",
                     "lt":"'${endDate}'",
                     "boost":2
                  }
               }
            }
         ]
      }
   },
   "aggs":{
      "distinct_hosts":{
         "terms":{
            "field":"HNFKPI.CompHost.keyword",
            "size":2000
         }
      }
   }
}' >>./queryResults/distinctHosts.json  
echo "Appending Results of the query to the file distinctHosts.json"

#<------------------------------------------------------------------>
#                 Construction of bash array for hosts
#<------------------------------------------------------------------>

echo "Constructing a bash array of distinct hosts"
hosts=($(jq <./queryResults/distinctHosts.json -r '.aggregations.distinct_hosts.buckets[].key|@sh' | tr -d \'\"))

#<------------------------------------------------------------------>
#                           Fetch distinct kpis
#<------------------------------------------------------------------>
echo "Fetching the list of unique kpis from elastic index $indexPattern & sorting them in ascending order"
curl -XPOST -H 'Content-Type: application/json' $elasticUrl$indexPattern"/_search?pretty" -d'
  {
   "size":0,
   "query":{
      "bool":{
         "must":[
            {
               "range":{
                  "@timestamp":{
                     "gte":"'${startDate}'",
                     "lte":"'${endDate}'",
                     "boost":2
                  }
               }
            }
         ]
      }
   },
   "aggs":{
      "kpisList":{
         "terms":{
            "field":"HNFKPI.KpiName.keyword",
            "size":2000,
            "order" : { "_key" : "asc" }
         }
      }
   }
}' >>./queryResults/kpis.json

echo "Appending Results of the query to the file hostKpis.json & componentKpis.json"

#<------------------------------------------------------------------>
#             Host & Component KPIs Array Construction
#<------------------------------------------------------------------>

echo "Constructing an array of distinct kpis for hosts"
IFS=$'\n' kpisList=($(jq <./queryResults/kpis.json -r '.aggregations.kpisList.buckets[].key|@sh' | tr -d \'))
echo "Host KPIs:-"

for kpi in "${kpisList[@]}"; do
  echo "KPI Name From Client :- $kpi"
  componentType=($(jq <kpiMappings.json -r --arg kpiName "$kpi" '.[]|select(.kpiNameFromClientData==$kpiName)|.kpiCategory|@sh' | tr -d \'\"))

  if test -z "$componentType"; then
    echo "KPI:- $kpi needs to be discovered in HEAL"

  elif [ $componentType == "host" ]; then
    echo "Component Type is host $kpi"
    kpisListForHosts+=("$kpi")

  fi

done



#<------------------------------------------------------------------------------>
#      Constructing CSV Headers & Rows Bash Variables for Host KPIs
#<------------------------------------------------------------------------------>

hostsCsvHeaders='["Timestamp"'
hostsCsvRows='[$ts'

echo "Host KPIs:- "
for kpi in "${kpisListForHosts[@]}"; do
    echo "$kpi"
    kpiNameInHeal=($(< $kpiMappingFile jq -r --arg kpiName "$kpi" '.[]|select(.kpiNameFromClientData==$kpiName)|.kpiNameInHeal|@sh'|tr -d \'\"))
    hostsCsvHeaders=''${hostsCsvHeaders}',"'${kpiNameInHeal}'"'
    hostsCsvRows=''${hostsCsvRows}',."'${kpi}'".kpiValue.value // 0'
done

hostsCsvHeaders=''${hostsCsvHeaders}']'
hostsCsvRows=''${hostsCsvRows}']'


hostKPIsCount=${#kpisListForHosts[@]}


#<----------------------------------------------------------------------------------------->
#              Aggregation Query to be appended for construction of Host CSVs
#<----------------------------------------------------------------------------------------->

for(( index=0; index< $hostKPIsCount; index++ )); do
    
    if [ $index -gt 0 ]; then
      finalListForHosts=','${finalListForHosts}' '
    fi
    
    kpi=${kpisListForHosts[$index]}
   
    finalListForHosts=' "'${kpisListForHosts[$index]}'":{ "filter":{ "bool":{ "must":{ "terms":{ "HNFKPI.KpiName.keyword":[ "'${kpisListForHosts[$index]}'" ] } } } }, "aggs":{ "kpiValue":{ "avg":{ "field":"HNFKPI.KpiValue" } } } } '${finalListForHosts}' ' 
    
done
echo "$finalListForHosts">./queryResults/hostKpisAggregationQuery.json


#<------------------------------------------------------------------>
#              Constructing & Invoking Query for Hosts
#<------------------------------------------------------------------>
curl -XPOST -H 'Content-Type: application/json' $elasticUrl$indexPattern"/_search?pretty" -d'
{
   "size":0,
   "query":{
      "bool":{
         "must":[
           {
              "range":{
                 "HNFKPI.TimestampFrom":{
                    "gte":"'${startDate}'",
                    "lte":"'${endDate}'",
                    "boost":2
                }
              }
           }
         ]
      }
   },
   "aggs":{
      "Timestamp":{
         "date_histogram":{
            "field":"HNFKPI.TimestampFrom",
            "interval":"1m"
         },
         "aggs":{
            "ServiceName":{
               "terms":{
                  "field":"HNFKPI.CompHost.keyword",
                  "size":"2000"
               },
               "aggs":{
                 '${finalListForHosts}'
               }
            }
         }
      }
   }
}' >>./queryResults/elasticAggregationQueryResultsForHosts.json


#<------------------------------------------------------------------>
#          Flattening hosts Aggregation Results to CSVs
#<------------------------------------------------------------------>

echo "Constructing the csv file for the hosts..."

for hostName in "${hosts[@]}"; do

  jq -r --arg host "$hostName" ''${hostsCsvHeaders}',(.aggregations.Timestamp.buckets[] | .key_as_string as $ts |.ServiceName.buckets[]|select(.key==$host)|'${hostsCsvRows}')|@csv' ./queryResults/elasticAggregationQueryResultsForHosts.json >./csvFiles/hostFiles/$hostName.csv
done

echo "Construction completed for the csv files for the hosts"


