export startDate='2020-08-25T14:20:00.000Z'         # start time for DSL range query
export endDate='2020-09-11T14:52:00.000Z'           # end time for DSL range query
export elasticUrl='http://localhost:9200/'          # elasticsearch url for script to connect to
export indexPattern='hnfm-host-*'            # index pattern of the elastic documents to search
export kpiMappingFile='kpiMappings.json'       # contains mapping between external KPIs and HEAL KPIs
export generateAll=false                        # if true script should generate csv for all hosts and component instances

 # Following fields are applicable only when generateAll is set to false

export csvFilesFor='host'                      # either 'host' or 'component'
export hostName='tdclpb6vva114.tdc.vzwcorp.com'            # list of host names for which csv need to be generated
#export instanceNames=("comp1", "comp2")        # list of instance names for which csv need to be generated
