#!/usr/bin/env bash
if [ -e "properties.sh" ]; then

  source properties.sh

  if [ "$0" = "$BASH_SOURCE" ]; then
    echo ""
    echo "Configuration Details: "
    echo ""

    echo "startData:$startDate endDate:$endDate"
    echo ""
    echo "elasticUrl:$elasticUrl indexPattern:$indexPattern"
    echo ""
    echo "kpiMappingFile:$kpiMappingFile"
    echo ""

    if [ -e $kpiMappingFile ]; then

      echo ""
      echo "Pinging elasticsearch instance: $elasticUrl"
      echo ""

      if [ "$(curl -Is -m 100 -XGET $elasticUrl | head -n 1)" = "HTTP/1.1 200 OK" ]; then
        echo "Connection to elasticsearch: successful"

        if [ $generateAll = false ]; then
          echo ""
          echo "Generating CSV files for: $csvFilesFor"
          echo ""
          if [ "$csvFilesFor" = "host" ]; then
             echo "hostNames:$hostNames"
          fi
          if [ "$csvFilesFor" = "component" ]; then
             echo "instanceNames:$instanceNames"
          fi
          echo ""
          ( exec ./generateSingleCSV.sh ) # execute in sub-shell
        else
          echo ""
          echo "Generating CSV files for all Hosts and Component Instances"
          echo ""
          ( exec ./generateMultipleCSVs.sh )  # execute in sub-shell
        fi
      else
        echo ""
        echo "Elasticsearch is not reachable"
        exit 1
      fi
    else
      echo ""
      echo "KPI mapping file not found"
      exit 1
    fi
  fi
else
  echo "Configuration file not found"
  exit 1
fi
