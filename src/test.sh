#!/bin/bash


#<---------------------------------------------------------------------------------------------------->
#         Removing the previous constructed results and creating new folders with thw same name
#<---------------------------------------------------------------------------------------------------->

rm -rf csvFiles
rm -rf queryResults
mkdir csvFiles
mkdir queryResults
mkdir ./csvFiles/hostFiles
mkdir ./csvFiles/componentFiles

#<------------------------------------------------------------------------------------->
#                   Initializing the timestamp range & Mappings File Path
#<------------------------------------------------------------------------------------->


#<------------------------------------------------------------------>
#                      Fetch distinct host names
#<------------------------------------------------------------------>

echo "Quering the elastic index heal-hnf-txn-* and fetching the list of distinct services"
curl -XPOST -H 'Content-Type: application/json' http://localhost:9200/heal-hnf-kpi/_search?pretty -d'
{
   "size":0,
   "query":{
      "bool":{
         "must":[
            {
               "range":{
                  "HNF.KPI.@timestamp":{
		     "gte":"'${startDate}'",
                     "lt":"'${endDate}'",
                     "boost":2
                  }
               }
            }
         ]
      }
   },
   "aggs":{
      "distinct_services":{
         "terms":{
            "field":"HNF.KPI.Component.Name.keyword",
            "size":2000
         }
      }
   }
}' >>./queryResults/distinctHosts.json  
echo "Appending Results of the query to the file distinctHosts.json"

#<------------------------------------------------------------------>
#                 Construction of bash array for hosts
#<------------------------------------------------------------------>

echo "Constructing a bash array of distinct hosts"
hosts=($(jq <./queryResults/distinctHosts.json -r '.aggregations.distinct_services.buckets[].key|@sh' | tr -d \'\"))

#<------------------------------------------------------------------->
#                  Component Instances Construction
##<------------------------------------------------------------------>
echo "Constructing a bash array of Component Instances by appending {jvm_1} with host names:- "
for host in "${hosts[@]}"; do
  componentInstances+=("$host"_jvm_1)
done
#<------------------------------------------------------------------>
#                           Fetch distinct kpis
#<------------------------------------------------------------------>
echo "Fetching the list of unique kpis from elastic index heal-hnf-kpi & sorting them in ascending order"
curl -XPOST -H 'Content-Type: application/json' http://localhost:9200/heal-hnf-kpi/_search?pretty -d'
  {
   "size":0,
   "query":{
      "bool":{
         "must":[
            {
               "range":{
                  "HNF.KPI.@timestamp":{
                     "gte":"'${startDate}'",
                     "lte":"'${endDate}'",
                     "boost":2
                  }
               }
            }
         ]
      }
   },
   "aggs":{
      "kpisList":{
         "terms":{
            "field":"HNF.KPI.Name.keyword",
            "size":2000,
            "order" : { "_key" : "asc" }
         }
      }
   }
}' >>./queryResults/kpis.json  

echo "Appending Results of the query to the file hostKpis.json & componentKpis.json"

#<------------------------------------------------------------------>
#             Host & Component KPIs Array Construction
#<------------------------------------------------------------------>

echo "Constructing an array of distinct kpis for hosts"
IFS=$'\n' kpisList=($(jq <./queryResults/kpis.json -r '.aggregations.kpisList.buckets[].key|@sh'|tr -d \'))
echo "Host KPIs:-"

for kpi in "${kpisList[@]}"; do
    echo "KPI Name From Client :- $kpi"
    componentType=($(<kpiMappings.json jq -r --arg kpiName "$kpi" '.[]|select(.kpiNameFromClientData==$kpiName)|.kpiCategory|@sh'|tr -d \'\"))
    
    if test -z "$componentType" 
    then
      echo "KPI:- $kpi needs to be discovered in HEAL"
    
    elif [ $componentType == "host" ]
    then 
      echo "Component Type is host $kpi"
      kpisListForHosts+=("$kpi")
      
    else
      echo "Component Type is component for $kpi"
      kpisListForInstances+=("$kpi")
      
fi
    
done

#<------------------------------------------------------------------------------>
#      Constructing CSV Headers & Rows Bash Variables for Host KPIs
#<------------------------------------------------------------------------------>

hostsCsvHeaders='["Timestamp"'
hostsCsvRows='[$ts'

echo "Host KPIs:- "
for kpi in "${kpisListForHosts[@]}"; do
    echo "$kpi"
    kpiNameInHeal=($(< $kpiMappingFile jq -r --arg kpiName "$kpi" '.[]|select(.kpiNameFromClientData==$kpiName)|.kpiNameInHeal|@sh'|tr -d \'\"))
    hostsCsvHeaders=''${hostsCsvHeaders}',"'${kpiNameInHeal}'"'
    hostsCsvRows=''${hostsCsvRows}',."'${kpi}'".kpiValue.value // 0'
done

hostsCsvHeaders=''${hostsCsvHeaders}']'
hostsCsvRows=''${hostsCsvRows}']'

#<---------------------------------------------------------------------------------->
#          Constructing CSV Headers & Rows Bash Variables for Component KPIs
#<---------------------------------------------------------------------------------->

componentCsvHeaders='["Timestamp"'
componentCsvRows='[$ts'

echo "Component Instance KPIs :- "
for kpi in "${kpisListForInstances[@]}"; do
    echo "$kpi"
    kpiNameInHeal=($(< $kpiMappingFile jq -r --arg kpiName "$kpi" '.[]|select(.kpiNameFromClientData==$kpiName)|.kpiNameInHeal|@sh'|tr -d \'\"))
    componentCsvHeaders=''${componentCsvHeaders}',"'${kpiNameInHeal}'"'
    componentCsvRows=''${componentCsvRows}',."'${kpi}'".kpiValue.value //0'
done

componentCsvHeaders=''${componentCsvHeaders}']'
componentCsvRows=''${componentCsvRows}']'



hostKPIsCount=${#kpisListForHosts[@]}
instanceKPIsCount=${#kpisListForInstances[@]}

finalListForHosts=''
finalListForInstances=''

#<----------------------------------------------------------------------------------------->
#              Aggregation Query to be appended for construction of Host CSVs
#<----------------------------------------------------------------------------------------->

for(( index=0; index< $hostKPIsCount; index++ )); do
    
    if [ $index -gt 0 ]; then
      finalListForHosts=','${finalListForHosts}' '
    fi
    
    kpi=${kpisListForHosts[$index]}
   
    kpiUnitTransformer=($(< $kpiMappingFile jq -r --arg kpiName "$kpi" '.[]|select(.kpiNameFromClientData==$kpiName)|.kpiUnit|@sh'|tr -d \'\"))
    percentageAsUnit=($(< $kpiMappingFile jq -r --arg kpiName "$kpi" '.[]|select(.kpiNameFromClientData==$kpiName)|.unitAsPercentage|@sh'|tr -d \'\"))
    echo "Unit Transformer Value : $kpiUnitTransformer ,Unit contains percentage : $percentageAsUnit KPI Name : $kpi"

    if [ "`echo "${kpiUnitTransformer} > 0" | bc`" -eq 1 ]; then
    
      if [ $percentageAsUnit == true ] ; then
        kpiUnitTransformer="$[ ${kpiUnitTransformer} * 100 ]"
      fi
      
      finalListForHosts=' "'${kpisListForHosts[$index]}'":{ "filter":{ "bool":{ "must":{ "terms":{ "HNF.KPI.Name.keyword":[ "'${kpisListForHosts[$index]}'" ] } } } }, "aggs":{ "kpiValue":{ "avg":{ "field":"HNF.KPI.Metric.Value", "script": {"lang": "painless","source": "_value * params.correction", "params": {"correction": '${kpiUnitTransformer}' }} } } } } '${finalListForHosts}' ' 
    #Division needs to be performed for Value Received From Client/ (Transformer Factor * -1.0)
    else
       
        kpiUnitTransformer="$[ ${kpiUnitTransformer} * -1 ]"
        
        if [ $percentageAsUnit == true ] ; then
          finalListForHosts=' "'${kpisListForHosts[$index]}'":{ "filter":{ "bool":{ "must":{ "terms":{ "HNF.KPI.Name.keyword":[ "'${kpisListForHosts[$index]}'" ] } } } }, "aggs":{ "kpiValue":{ "avg":{ "field":"HNF.KPI.Metric.Value", "script": {"lang": "painless","source": "(_value *100)/ params.correction", "params": {"correction": '${kpiUnitTransformer}' }} } } } } '${finalListForHosts}' ' 
        
        else
          finalListForHosts=' "'${kpisListForHosts[$index]}'":{ "filter":{ "bool":{ "must":{ "terms":{ "HNF.KPI.Name.keyword":[ "'${kpisListForHosts[$index]}'" ] } } } }, "aggs":{ "kpiValue":{ "avg":{ "field":"HNF.KPI.Metric.Value", "script": {"lang": "painless","source": "_value / params.correction", "params": {"correction": '${kpiUnitTransformer}' }} } } } } '${finalListForHosts}' ' 
        fi  
        
  fi
    
done
echo "$finalListForHosts">./queryResults/hostKpisAggregationQuery.json

#<-------------------------------------------------------------------------------------->
#          Aggregation Query to be appended for construction of Instance CSVs
#<-------------------------------------------------------------------------------------->

for(( index=0; index< $instanceKPIsCount; index++ )); do
    
    if [ $index -gt 0 ]; then
      finalListForInstances=','${finalListForInstances}' '
    fi
    
    kpi=${kpisListForInstances[$index]}
   
    kpiUnitTransformer=($(< $kpiMappingFile jq -r --arg kpiName "$kpi" '.[]|select(.kpiNameFromClientData==$kpiName)|.kpiUnit|@sh'|tr -d \'\"))
    percentageAsUnit=($(< $kpiMappingFile jq -r --arg kpiName "$kpi" '.[]|select(.kpiNameFromClientData==$kpiName)|.unitAsPercentage|@sh'|tr -d \'\"))
    
    echo "Unit Transformer Factor : $kpiUnitTransformer , Do Unit contains percentage : $percentageAsUnit, KPI Name : $kpi"

    if [ "`echo "${kpiUnitTransformer} > 0" | bc`" -eq 1 ]; then
    
      if [ $percentageAsUnit == true ] ; then
        kpiUnitTransformer="$[ ${kpiUnitTransformer} * 100 ]"
      fi
      
      finalListForInstances=' "'${kpisListForInstances[$index]}'":{ "filter":{ "bool":{ "must":{ "terms":{ "HNF.KPI.Name.keyword":[ "'${kpisListForInstances[$index]}'" ] } } } }, "aggs":{ "kpiValue":{ "avg":{ "field":"HNF.KPI.Metric.Value", "script": {"lang": "painless","source": "_value * params.correction", "params": {"correction": '${kpiUnitTransformer}' }} } } } } '${finalListForInstances}' ' 
    #Division needs to be performed for Value Received From Client/ (Transformer Factor * -1.0)
    else
       
        kpiUnitTransformer="$[ ${kpiUnitTransformer} * -1 ]"
        
        if [ $percentageAsUnit == true ] ; then
          finalListForInstances=' "'${kpisListForInstances[$index]}'":{ "filter":{ "bool":{ "must":{ "terms":{ "HNF.KPI.Name.keyword":[ "'${kpisListForInstances[$index]}'" ] } } } }, "aggs":{ "kpiValue":{ "avg":{ "field":"HNF.KPI.Metric.Value", "script": {"lang": "painless","source": "(_value *100)/ params.correction", "params": {"correction": '${kpiUnitTransformer}' }} } } } } '${finalListForInstances}' ' 
        
        else
          finalListForInstances=' "'${kpisListForInstances[$index]}'":{ "filter":{ "bool":{ "must":{ "terms":{ "HNF.KPI.Name.keyword":[ "'${kpisListForInstances[$index]}'" ] } } } }, "aggs":{ "kpiValue":{ "avg":{ "field":"HNF.KPI.Metric.Value", "script": {"lang": "painless","source": "_value / params.correction", "params": {"correction": '${kpiUnitTransformer}' }} } } } } '${finalListForInstances}' ' 
        fi  
        
  fi
    
done

echo "$finalListForInstances">./queryResults/componentKpisAggregationQuery.json

#<------------------------------------------------------------------>
#         Constructing Final Elastic Query For Instances
#<------------------------------------------------------------------>

finalListForInstances='{
   "size":0,
   "query":{
      "bool":{
         "must":[
           {
              "range":{
                 "HNF.KPI.@timestamp":{
                    "gte":"'${startDate}'",
                    "lte":"'${endDate}'",
                    "boost":2
                }
              }
           }
         ]
      }
   },
   "aggs":{
      "Timestamp":{
         "date_histogram":{
            "field":"HNF.KPI.@timestamp",
            "interval":"1m"
         },
         "aggs":{
            "ServiceName":{
               "terms":{
                  "field":"HNF.KPI.Component.Name.keyword",
                  "size":"2000"
               },
               "aggs":{
                 '${finalListForInstances}'
               }
               
            }
         }
      }
   }
}'

#<------------------------------------------------------------------>
#                 Invoking Query for Instances
#<------------------------------------------------------------------>
echo "Quering the elastic index heal-hnf-kpi and fetching the data on which several levels of categorizations are performed"

echo "Appending Results of the query to the file elasticAggregationQueryResults.json"

curl -XPOST -H 'Content-Type: application/json' http://localhost:9200/heal-hnf-kpi/_search?pretty -d "${finalListForInstances}">>./queryResults/elasticAggregationQueryResultsForInstances.json

#<------------------------------------------------------------------>
#              Constructing & Invoking Query for Hosts
#<------------------------------------------------------------------>
curl -XPOST -H 'Content-Type: application/json' http://localhost:9200/heal-hnf-kpi/_search?pretty -d'
{
   "size":0,
   "query":{
      "bool":{
         "must":[
           {
              "range":{
                 "HNF.KPI.@timestamp":{
                    "gte":"'${startDate}'",
                    "lte":"'${endDate}'",
                    "boost":2
                }
              }
           }
         ]
      }
   },
   "aggs":{
      "Timestamp":{
         "date_histogram":{
            "field":"HNF.KPI.@timestamp",
            "interval":"1m"
         },
         "aggs":{
            "ServiceName":{
               "terms":{
                  "field":"HNF.KPI.Component.Name.keyword",
                  "size":"2000"
               },
               "aggs":{
                 '${finalListForHosts}'
               }
            }
         }
      }
   }
}' >>./queryResults/elasticAggregationQueryResultsForHosts.json


#<------------------------------------------------------------------>
#          Flattening hosts Aggregation Results to CSVs
#<------------------------------------------------------------------>

echo "Constructing the csv file for the hosts..."

for hostName in "${hosts[@]}"; do
 
  jq -r --arg host "${hostName}" ''${hostsCsvHeaders}',(.aggregations.Timestamp.buckets[] | .key_as_string as $ts |.ServiceName.buckets[]|select(.key==$host)|'${hostsCsvRows}')|@csv' ./queryResults/elasticAggregationQueryResultsForHosts.json >./csvFiles/hostFiles/$hostName.csv
done

echo "Construction completed for the csv files for the hosts"


#<------------------------------------------------------------------------------->
#            Flattening component Instances Aggregation Results to CSVs
#<------------------------------------------------------------------------------->

echo "Constructing the csv file for the instances...."

for hostName in "${hosts[@]}"; do

  jq -r --arg hostName "${hostName}" ''${componentCsvHeaders}',(.aggregations.Timestamp.buckets[] | .key_as_string as $ts |.ServiceName.buckets[]|select(.key==$hostName)|'${componentCsvRows}')|@csv' ./queryResults/elasticAggregationQueryResultsForInstances.json >./csvFiles/componentFiles/"$hostName"_jvm_1.csv
done

echo "Construction completed for the csv file for the instances"
