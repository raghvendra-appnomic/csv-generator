#!/usr/bin/env bash

# This script generates cvs files for all component and hosts

source properties.sh

#<---------------------------------------------------------------------------------------------------->
#         Removing the previous constructed results and creating new folders with thw same name
#<---------------------------------------------------------------------------------------------------->

rm -rf csvFiles
rm -rf queryResults
mkdir csvFiles
mkdir queryResults
mkdir ./csvFiles/hostFiles
mkdir ./csvFiles/componentFiles